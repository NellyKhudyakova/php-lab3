<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №A3</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №A3</h1></div>
    <div></div>
</header>

<main>
    <div class="numbers-panel">
        <?php

        if ( !isset($_GET['store']) )
            $_GET['store'] = '';
        else if (isset($_GET['key']))
            $_GET['store'] .= $_GET['key'];

        echo '<div class="line1 result">'.$_GET['store'].'</div>';
        ?>

        <div class="line2">
            <a href="?key=1&store=<?php echo $_GET['store']; ?>">1</a>
            <a href="?key=2&store=<?php echo $_GET['store']; ?>">2</a>
            <a href="?key=3&store=<?php echo $_GET['store']; ?>">3</a>
            <a href="?key=4&store=<?php echo $_GET['store']; ?>">4</a>
            <a href="?key=5&store=<?php echo $_GET['store']; ?>">5</a>
        </div >
        <div class="line3">
            <a href="?key=6&store=<?php echo $_GET['store']; ?>">6</a>
            <a href="?key=7&store=<?php echo $_GET['store']; ?>">7</a>
            <a href="?key=8&store=<?php echo $_GET['store']; ?>">8</a>
            <a href="?key=9&store=<?php echo $_GET['store']; ?>">9</a>
            <a href="?key=0&store=<?php echo $_GET['store']; ?>">0</a>
        </div>
        <div class="line4">
            <a href="/php-lab3/index.php" class="delete">СБРОС</a>
        </div>

    </div>

</main>

<footer>

</footer>

</body>
</html>